# Frontend Developer Tech Test
In this tech test for this SpaceX Launch project, I followed these steps to create the webpage:

I started by creating a rough draft of the webpage using Figma. You can find the design here: https://www.figma.com/file/6Pp4YFmixbzIqgcbPssDU6/Untitled?type=design&node-id=0-1&mode=design&t=xO3NH7lFoCUj8Dm8-0. This helped me visualize the layout and elements I needed for the project.

Then I proceeded with the development phase where I used Material UI to leverage its pre-built components (Pagination, Grid, Circular Progress, etc.)

Also, I saw that the launch object had more items other than the ones displayed on the card, so I proceeded to add a modal on click of any of the cards, and these modals hold extra information about each of the cards. Please click on any of the cards to see the modal. 

I deployed the project to vercel. You can find the link here: https://space-x-launch-xi.vercel.app/

Overall, my goal was to deliver a well-organized, visually appealing, and functional frontend application.
