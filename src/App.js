import './App.css';
import { useState, useEffect } from "react";
import Launches from "./components/Launches";
import {
    Box,
    CircularProgress,
    Container,
    Pagination,
    useTheme,
    useMediaQuery,
} from "@mui/material";
import styles from './components/Launches/launches.module.css';
import LaunchModal from "./components/LaunchModal";

function App() {

    const [data, setData] = useState([]);
    const [modalData, setModalData] = useState([]);
    const [currentPage, setCurrentPage] = useState(1)
    const apiUrl = 'https://api.spacexdata.com/v4/launches/query';
    const theme = useTheme();
    const isSmallScreen = useMediaQuery(theme.breakpoints.down('sm'));
    const paginationSize = isSmallScreen ? 'small' : 'large';

    function getQueryBody(pageNumber) {
        return  {
            query: {
                upcoming: false,
                success: true
            },
            options: {
                page: pageNumber,
                select: {
                    id: 1,
                    name: 2,
                    links: 3,
                    date_utc: 4,
                    flight_number: 5,
                },
                populate: [
                    {
                        path: 'rocket',
                        select: {
                            id: 1,
                            name: 2,
                            type: 3,
                            description: 4,
                            height: 5,
                            diameter: 6,
                            mass: 7,
                            flickr_images: 8,
                        },
                    },
                    {
                        path: 'crew',
                        select: {
                            id: 1,
                            name: 2,
                            agency: 3,
                            image: 4,
                        },
                    },
                    {
                        path: 'payloads',
                        select: {
                            id: 1,
                            name: 2,
                            type: 3,
                            orbit: 4,
                            reference_system: 5,
                            regime: 6
                        }
                    },
                    {
                        path: 'capsules',
                        select: {
                            id: 1,
                            type: 2,
                            status: 3,
                            serial: 4
                        }
                    },
                    {
                        path: 'launchpad',
                        select: {
                            id: 1,
                            name: 2,
                            full_name: 3,
                            locality: 4,
                            region: 5,
                            latitude: 6,
                            longitude: 7,
                            details: 8
                        }
                    }
                ],
                sort: {
                    flight_number: 'desc',
                },
            },
        };

    }

    const fetchData = async (pageNumber) => {
        try {
            const response = await fetch(apiUrl, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(getQueryBody(pageNumber)),
            });

            if (!response.ok) {
                console.log('Network response was not ok');
            }

            const responseData = await response.json();
            setData(responseData);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    useEffect(() => {
        fetchData(currentPage);
    }, [currentPage]);

    const handlePageChange = (event, page) => {
        setCurrentPage(page);
    };

    const [open, setOpen] = useState(false);

    const handleClose = () => {
        setOpen(false);
    };

    const handleLaunchesClick = (item) => {
        setOpen(true);
        setModalData(item);
    }

    return (
        <div>
            <Container>
                {data["docs"] ? (
                    <div>
                        <p className={styles.totalLaunches}>Total Launches: {data["totalDocs"]}</p>

                        <Launches launches={data["docs"]} handleLaunchesClick={handleLaunchesClick}/>
                        <Pagination
                            count={data["totalPages"]}
                            page={currentPage}
                            onChange={handlePageChange}
                            showFirstButton showLastButton
                            color='primary'
                            size={paginationSize}
                            sx={{
                                display: 'flex',
                                justifyContent: 'center',
                                marginTop: '34px',
                                width: '100%',
                                '& .MuiPaginationItem-root': {
                                    color: '#FFFFFF',
                                },
                            }}
                        />

                       <LaunchModal open={open} modalData={modalData} handleClose={handleClose}/>
                   </div>
                ) : (
                    <Box
                        display="flex"
                        justifyContent="center"
                        alignItems="center"
                        height="100vh"
                    >
                        <CircularProgress />
                    </Box>
                )}
            </Container>
        </div>
    );
}
export default App;
