import styles from './launch.module.css'
import moment from 'moment'

const Launch = ({props}) => {

    return (
        <div className={styles.launchCard}>
            <div className={styles.launchFlightNumber}>
                <span>Flight No: {props.flight_number}</span>
            </div>
            <img
                className={styles.launchImg}
                src={props?.links?.patch?.small}
                height='104px'
                width='104px'

            />
            <div className={styles.launchCardContent}>
                <div className={styles.launchTitle}>{props.name}</div>
                <div className={styles.launchId}>Id: {props.id}</div>
                <div className={styles.launchId}>Flight Number: {props.flight_number}</div>
                <div className={styles.launchId}>Launch Date: {moment(props.date_utc).format('MM.DD.YYYY, h:mm:ss A')}</div>
            </div>
        </div>
    )

}

export default Launch