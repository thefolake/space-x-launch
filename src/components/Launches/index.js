import { Grid } from "@mui/material";
import Launch from "../Launch";
import styles from './launches.module.css';

const Launches = ({ launches, handleLaunchesClick }) =>  {
    return (
        <Grid container spacing={2} >
            {launches.map((launch) => (
                <Grid key={launch.id} item xs={12} sm={6} md={4} lg={2.4} onClick={() => handleLaunchesClick(launch)}>
                    <div className={styles.page}>
                        <Launch props={launch}/>
                    </div>
                </Grid>
            ))}
        </Grid>
    );
}


export default Launches