import { Box, CircularProgress, Grid, Modal } from "@mui/material";
import styles from "./launchModal.module.css";
import moment from "moment";

const LaunchModal = ({ open, handleClose, modalData}) => {
    return (
        <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            {
                modalData ?
                    <Box className={styles.launchModal}>
                        <img
                            src={modalData?.links?.patch?.small}
                            height='207px'
                            width='207px'

                        />

                        <div className={styles.modalContent}>
                            <Grid container rowSpacing={2} columnSpacing={{ xs: 2, sm: 3, md: 6 }}>
                                <Grid item xs={12} sm={6}>
                                    <div className={styles.modalContentHeading}>Launch Name</div>
                                    <div className={styles.modalContentText}>{modalData?.name}</div>
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <div className={styles.modalContentHeading}>ID</div>
                                    <div className={styles.modalContentText}>{modalData?.id}</div>
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <div className={styles.modalContentHeading}>Flight Number</div>
                                    <div className={styles.modalContentText}>{modalData?.flight_number}</div>
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <div className={styles.modalContentHeading}>Launch Date</div>
                                    <div className={styles.modalContentText}>{moment(modalData?.date_utc).format('MM.DD.YYYY, h:mm:ss A')}</div>
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <div className={styles.modalContentHeading}>Launch Pad Name</div>
                                    <div className={styles.modalContentText}>{modalData?.launchpad?.name}</div>
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <div className={styles.modalContentHeading}>Launch Pad Full Name</div>
                                    <div className={styles.modalContentText}>{modalData?.launchpad?.full_name}</div>
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <div className={styles.modalContentHeading}>Launch Pad Locality</div>
                                    <div className={styles.modalContentText}>{modalData?.launchpad?.locality}</div>
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <div className={styles.modalContentHeading}>Launch Pad Region</div>
                                    <div className={styles.modalContentText}>{modalData?.launchpad?.region}</div>
                                </Grid>
                            </Grid>
                        </div>
                    </Box> :
                    <Box
                        display="flex"
                        justifyContent="center"
                        alignItems="center"
                        height="100vh"
                    >
                        <CircularProgress />
                    </Box>
            }

        </Modal>
    )
};

export default LaunchModal